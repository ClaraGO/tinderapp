package fr.utt.if26.tinderapp;

import android.content.Context;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;

public class Personne {
    private int id;

    @SerializedName("name")
    @Expose
    private String nom;

    private String prenom;
    private String date_naissance; // jj/mm/aaaa
    private String sexe; // m ou f
    private String pseudo;
    private String description;
    private String longitude;
    private String latitude;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("url")
    @Expose
    private String imageUrl;

    private int preference; // 0 homme, 1 femme, 2 les deux
    private int age_min;
    private int age_max;

    @SerializedName("age")
    @Expose
    private int age;

    private int distance_max;
    private ArrayList<Personne> liked_persons;
    private ArrayList<String> photos;
    // Photos encodées en base 64 à terme

    public Personne(){

    }

    public Personne(int id, String nom, String prenom, String date_naissance, String sexe, String pseudo, String description, String longitude, String latitude, int preference, int age_min, int age_max, int distance_max, ArrayList<Personne> liked_persons, ArrayList<String> photos) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.date_naissance = date_naissance;
        this.sexe = sexe;
        this.pseudo = pseudo;
        this.description = description;
        this.longitude = longitude;
        this.latitude = latitude;
        this.preference = preference;
        this.age_min = age_min;
        this.age_max = age_max;
        this.distance_max = distance_max;
        this.liked_persons = liked_persons;
        this.photos = photos;
    }

    public int getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom, Context c) {
        this.nom = nom;
        DatabaseHelper db = new DatabaseHelper(c);
        db.updateSettingsString(this.id, nom, "nom");
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom, Context c, DatabaseHelper db) {
        this.prenom = prenom;
        db.updateSettingsString(this.id, prenom, "prenom");
    }

    public String getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(String date_naissance, Context c) {
        this.date_naissance = date_naissance;
        DatabaseHelper db = new DatabaseHelper(c);
        db.updateSettingsString(this.id, date_naissance, "date_naissance");
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo, Context c) {
        this.pseudo = pseudo;
        DatabaseHelper db = new DatabaseHelper(c);
        db.updateSettingsString(this.id, pseudo, "pseudo");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description, Context c, DatabaseHelper db) {
        this.description = description;
        db.updateSettingsString(this.id, description, "description");
    }

    public String getLongitude() {
        return longitude;
    }

    /*public void setLongitude(String longitude) {
        this.longitude = longitude;
    }*/

    public String getLatitude() {
        return latitude;
    }

    /*public void setLatitude(String latitude) {
        this.latitude = latitude;
    }*/

    public int getPreference() {
        return preference;
    }

    public void setPreference(int preference, Context c, DatabaseHelper db) {
        this.preference = preference;
        db.updateSettingsInt(this.id, preference, "preference");
    }

    public int getAge_min() {
        return age_min;
    }

    public void setAge_min(int age_min, Context c, DatabaseHelper db) {
        this.age_min = age_min;
        db.updateSettingsInt(this.id, age_min, "age_min");
    }

    public int getAge_max() {
        return age_max;
    }

    public void setAge_max(int age_max, Context c, DatabaseHelper db) {
        this.age_max = age_max;
        db.updateSettingsInt(this.id, age_max, "age_max");
    }

    /*public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }*/

    public int getDistance_max() {
        return distance_max;
    }

    public void setDistance_max(int distance_max, Context c, DatabaseHelper db) {
        this.distance_max = distance_max;
        db.updateSettingsInt(this.id, distance_max, "distance_max");
    }

    public ArrayList<Personne> getLiked_persons() {
        return liked_persons;
    }

    public void setLiked_persons(ArrayList<Personne> liked_persons) {
        this.liked_persons = liked_persons;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getAge() {
        return age;
    }

    public ArrayList<String> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<String> initial_pictures, ArrayList<String> imported_images, Context c, DatabaseHelper db){
        initial_pictures.removeAll(Collections.singleton(null));
        imported_images.removeAll(Collections.singleton(null));
        if(initial_pictures.size() == (imported_images.size())){
            for(int i = 0; i < initial_pictures.size(); i++){
                if(!initial_pictures.get(i).equals(imported_images.get(i))){
                    //update l'image liée à la position dans BD
                    db.updatePictureAtPosition(id, i+1, imported_images.get(i));
                }
            }
        }
        else if(imported_images.size() > initial_pictures.size()){
            int initial_size = initial_pictures.size();
            for(int i = 0;i < initial_pictures.size(); i++){
                if(!initial_pictures.get(i).equals(imported_images.get(i))){
                    //update l'image liée à la position dans BD
                    db.updatePictureAtPosition(id, i+1, imported_images.get(i));
                }
            }
            for(int j = initial_size; j < imported_images.size(); j++){
                //insert image de position j
                if(imported_images.get(j) != null){
                    db.insertPictureAtPosition(id, j+1, imported_images.get(j));
                }
            }
        }
        else {//veut dire qu'yen a moins
            //Dabord check sil faut update les premieres images,
            int initial_size = initial_pictures.size();//> a final size
            int final_size = imported_images.size();
            for(int i = 0; i < imported_images.size(); i++){
                if(!imported_images.get(i).equals(initial_pictures.get(i))){
                    //update
                    db.updatePictureAtPosition(id, i+1, imported_images.get(i));
                }
            }
            //then loop dans les restantes en les supprimant
            for(int j = final_size; j < initial_pictures.size(); j++){
                //delete de BD
                db.deletePictureAtPosition(id, j+1);
            }
        }
        this.photos = imported_images;
    }
}
