package fr.utt.if26.tinderapp;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MatchsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MatchsFragment extends Fragment {
    // Store instance variables
    private ListView list;
    private Personne mPersonneActuelle;
    private ArrayList<Personne> matchs;

    ArrayList<String> prenoms;
    ArrayList<String> photos;

    // newInstance constructor for creating fragment with arguments
    public static MatchsFragment newInstance(int page, String title) {
        MatchsFragment matchsFragment = new MatchsFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        matchsFragment.setArguments(args);
        return matchsFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPersonneActuelle = GlobalClass.getAuthenticated_user();
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());
        matchs = db.getMatchFromPersonneId(this.mPersonneActuelle.getId());
        prenoms = new ArrayList<String>();
        photos = new ArrayList<String>();
        for (Personne personne : matchs) {
            prenoms.add(personne.getPrenom());
            photos.add(personne.getPhotos().get(0));
        }
        View view = inflater.inflate(R.layout.fragment_matchs, container, false);
        MatchsListView adapter = new MatchsListView(getActivity(), prenoms, photos);
        list = (ListView) view.findViewById(R.id.list);
        list.setAdapter(adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int idMatch = matchs.get(position).getId();
                Intent intent = new Intent(getActivity().getApplicationContext(), TinderCardActivity.class);
                Bundle b = new Bundle();
                b.putInt("id", idMatch); //Your id
                intent.putExtras(b); //Put your id to your next Intent
                getActivity().startActivity(intent);
            }
        });

        return view;
    }

    public void updateAdapter() {
        DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());
        matchs = db.getMatchFromPersonneId(this.mPersonneActuelle.getId());
        prenoms = new ArrayList<String>();
        photos = new ArrayList<String>();
        for (Personne personne : matchs) {
            prenoms.add(personne.getPrenom());
            photos.add(personne.getPhotos().get(0));
        }
        MatchsListView adapter = new MatchsListView(getActivity(), prenoms, photos);
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
