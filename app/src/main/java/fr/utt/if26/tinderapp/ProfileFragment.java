package fr.utt.if26.tinderapp;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // Store instance variables
    private String title;
    private int page;

    private DatabaseHelper db;
    private Personne personne;

    private RangeSeekBar seekBar;
    private TextView age_min_tv;
    private TextView age_max_tv;
    private int age_min;
    private int age_max;
    private boolean age_range_changed = false;

    private EditablePicture editable_picture_1;
    private Button add_picture_button_1;
    private Button delete_picture_button_1;
    private EditablePicture editable_picture_2;
    private Button add_picture_button_2;
    private Button delete_picture_button_2;
    private EditablePicture editable_picture_3;
    private Button add_picture_button_3;
    private Button delete_picture_button_3;
    private EditablePicture editable_picture_4;
    private Button add_picture_button_4;
    private Button delete_picture_button_4;
    private int button_clicked = 0;
    private Boolean[] images_filled = new Boolean[4];
    private ArrayList<String> pictures;
    private ArrayList<String> imported_images;

    public static final int GET_FROM_GALLERY = 1;

    private EditText prenom_et;
    private String prenom;
    private boolean prenom_changed = false;

    private EditText description_et;
    private String description;
    private boolean description_changed = false;

    private TextView distance_value;
    private SeekBar distance_seekbar;
    private boolean distance_changed = false;

    private CheckBox hommes_checkbox, femmes_checkbox;
    private boolean hommes_checked = false, femmes_checked = false, preference_changed = false;

    private Button save_modifications;

    // newInstance constructor for creating fragment with arguments
    public static ProfileFragment newInstance(int page, String title) {
        ProfileFragment profileFragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        profileFragment.setArguments(args);

        return profileFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");

        db = new DatabaseHelper(getActivity());

        personne = GlobalClass.getAuthenticated_user();


        prenom = personne.getPrenom();
        description = personne.getDescription();

        int recherche = personne.getPreference();
        switch (recherche) {
            case 0:
                hommes_checked = true;
                break;
            case 1:
                femmes_checked = true;
                break;
            case 2:
                hommes_checked = true;
                femmes_checked = true;
        }


        age_min = personne.getAge_min();
        age_max = personne.getAge_max();

        pictures = db.getPhotosFromPersonId(personne.getId());
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        // TextView tvLabel = (TextView) view.findViewById(R.id.profile_label);
        // tvLabel.setText(page + " -- " + title);

        imported_images = new ArrayList<String>();
        Arrays.fill(images_filled, false);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pictures = personne.getPhotos();
        //Upload image
        editable_picture_1 = getView().findViewById(R.id.editable_picture_1);
        ImageView iv1 = editable_picture_1.getImageView();
        add_picture_button_1 = editable_picture_1.getAdd_button();
        delete_picture_button_1 = editable_picture_1.getDelete_button();

        addListenerOnAddButton(add_picture_button_1, 1);
        addListenerOnDeleteButton(delete_picture_button_1, 1);

        editable_picture_2 = getView().findViewById(R.id.editable_picture_2);
        ImageView iv2 = editable_picture_2.getImageView();
        add_picture_button_2 = editable_picture_2.getAdd_button();
        delete_picture_button_2 = editable_picture_2.getDelete_button();


        addListenerOnAddButton(add_picture_button_2, 2);
        addListenerOnDeleteButton(delete_picture_button_2, 2);


        editable_picture_3 = getView().findViewById(R.id.editable_picture_3);
        ImageView iv3 = editable_picture_3.getImageView();
        add_picture_button_3 = editable_picture_3.getAdd_button();
        delete_picture_button_3 = editable_picture_3.getDelete_button();


        addListenerOnAddButton(add_picture_button_3, 3);
        addListenerOnDeleteButton(delete_picture_button_3, 3);


        editable_picture_4 = getView().findViewById(R.id.editable_picture_4);
        ImageView iv4 = editable_picture_4.getImageView();
        add_picture_button_4 = editable_picture_4.getAdd_button();
        delete_picture_button_4 = editable_picture_4.getDelete_button();


        addListenerOnAddButton(add_picture_button_4, 4);
        addListenerOnDeleteButton(delete_picture_button_4, 4);


        if (pictures.size() >= 1) {
            iv1.setImageDrawable(getDrawableFromEncodedString(pictures.get(0)));
            imported_images.add(pictures.get(0));
            imported_images.add(null);
            imported_images.add(null);
            imported_images.add(null);
            images_filled[0] = true;
        }
        if (pictures.size() >= 2) {
            iv2.setImageDrawable(getDrawableFromEncodedString(pictures.get(1)));
            imported_images.set(1, pictures.get(1));
            images_filled[1] = true;
        }
        if (pictures.size() >= 3) {
            iv3.setImageDrawable(getDrawableFromEncodedString(pictures.get(2)));
            imported_images.set(2, pictures.get(2));
            images_filled[2] = true;
        }
        if (pictures.size() >= 4) {
            iv4.setImageDrawable(getDrawableFromEncodedString(pictures.get(3)));
            imported_images.set(3, pictures.get(3));
            images_filled[3] = true;
        }


        // create RangeSeekBar as Integer range between 20 and 75
        seekBar = new RangeSeekBar(18, 45, getActivity());
        seekBar.setSelectedMinValue(age_min);
        seekBar.setSelectedMaxValue(age_max);

        addListenerOnDoubleSeekBar(seekBar);

// add RangeSeekBar to pre-defined layout
        ViewGroup layout = (ViewGroup) getView().findViewById(R.id.age_layout);
        layout.addView(seekBar);


        //prenom
        prenom_et = getView().findViewById(R.id.prenom_et);
        prenom_et.setText(prenom);

        addListenerOnPrenom(prenom_et);

        //description
        description_et = getView().findViewById(R.id.description_et);
        description_et.setText(description);

        addListenerOnDescription(description_et);

        //Hommes/femmes checkboxes
        hommes_checkbox = getView().findViewById(R.id.hommes_checkbox);
        femmes_checkbox = getView().findViewById(R.id.femmes_checkbox);
        hommes_checkbox.setChecked(hommes_checked);
        femmes_checkbox.setChecked(femmes_checked);


        //age range
        age_min_tv = getView().findViewById(R.id.age_min_tv);
        age_max_tv = getView().findViewById(R.id.age_max_tv);
        age_min_tv.setText(String.valueOf(age_min));
        age_max_tv.setText(String.valueOf(age_max));

        //Distance max
        distance_value = getView().findViewById(R.id.distance_value_tv);
        distance_seekbar = getView().findViewById(R.id.distance_seekbar);

        addListenerOnHommesCheckbox(hommes_checkbox);
        addListenerOnFemmesCheckbox(femmes_checkbox);


        String value = String.valueOf(personne.getDistance_max()) + " km";
        distance_seekbar.setProgress(personne.getDistance_max());
        distance_value.setText(value);

        addListenerOnDistanceSeekbar(distance_seekbar);


        save_modifications = getView().findViewById(R.id.modifications_button);
        addListenerOnSaveModificationsbutton(save_modifications);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GET_FROM_GALLERY && resultCode == Activity.RESULT_OK) {

            Uri selectedImage = data.getData();
          /*  String[] filePathColumn = {MediaStore.Images.Media.DATA, MediaStore.Images.Media.DISPLAY_NAME};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    //getApplicationContext().getContentResolver().

            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String filePath = cursor.getString(columnIndex);
                System.out.println(filePath);
               // Bitmap bitmap = BitmapFactory.decodeFile(filePath);
            }
            cursor.close();
*/


            Bitmap bitmap = null;
            try {
                Bitmap tempBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);

                System.out.println(tempBitmap.getWidth() + "    " + tempBitmap.getHeight());
                if(tempBitmap.getHeight() >= 1000){
                    bitmap = getResizedBitmap(tempBitmap, tempBitmap.getHeight()/10, tempBitmap.getWidth()/10);
                }
                else{
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                }

                System.out.println(bitmap.getWidth() +"     "+ bitmap.getHeight());

                RoundedBitmapDrawable roundDrawable = getDrawableFromBitmap(bitmap);
                switch (button_clicked) {
                    case 1:
                        editable_picture_1.getImageView().setImageDrawable(roundDrawable);
                        imported_images.set(0, encodeBitmapToBase64(bitmap));
                        break;
                    case 2:
                        if (images_filled[0]) {
                            editable_picture_2.getImageView().setImageDrawable(roundDrawable);
                            images_filled[1] = true;
                            imported_images.set(1, encodeBitmapToBase64(bitmap));
                        } else {
                            editable_picture_1.getImageView().setImageDrawable(roundDrawable);
                            images_filled[0] = true;
                            imported_images.set(0, encodeBitmapToBase64(bitmap));
                        }
                        break;
                    case 3:
                        if (images_filled[0] && images_filled[1]) {
                            editable_picture_3.getImageView().setImageDrawable(roundDrawable);
                            images_filled[2] = true;
                            imported_images.set(2, encodeBitmapToBase64(bitmap));

                        } else if (images_filled[0]) {
                            editable_picture_2.getImageView().setImageDrawable(roundDrawable);
                            images_filled[1] = true;
                            imported_images.set(1, encodeBitmapToBase64(bitmap));

                        } else {
                            editable_picture_1.getImageView().setImageDrawable(roundDrawable);
                            images_filled[0] = true;
                            imported_images.set(0, encodeBitmapToBase64(bitmap));

                        }
                        break;
                    case 4:
                        if (images_filled[2] && images_filled[1] && images_filled[0]) {
                            editable_picture_4.getImageView().setImageDrawable(roundDrawable);
                            images_filled[3] = true;
                            imported_images.set(3, encodeBitmapToBase64(bitmap));

                        } else if (images_filled[1] && images_filled[0]) {
                            editable_picture_3.getImageView().setImageDrawable(roundDrawable);
                            images_filled[2] = true;
                            imported_images.set(2, encodeBitmapToBase64(bitmap));

                        } else if (images_filled[0]) {
                            editable_picture_2.getImageView().setImageDrawable(roundDrawable);
                            images_filled[1] = true;
                            imported_images.set(1, encodeBitmapToBase64(bitmap));

                        } else {
                            editable_picture_1.getImageView().setImageDrawable(roundDrawable);
                            images_filled[0] = true;
                            imported_images.set(0, encodeBitmapToBase64(bitmap));

                        }
                        break;
                }
            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        // create a matrix for the manipulation
        Matrix matrix = new Matrix();

        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);

        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;
    }

    public RoundedBitmapDrawable getDrawableFromBitmap(Bitmap bm) {
        RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), bm);
        //roundDrawable.setCircular(true);
        final float roundPx = (float) bm.getWidth() * 0.06f;
        roundDrawable.setCornerRadius(roundPx);
        return roundDrawable;
    }

    public RoundedBitmapDrawable getDrawableFromEncodedString(String encodedString) {
        byte[] decodedBytes = Base64.decode(encodedString, 0);
        Bitmap bm = BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
        RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), bm);
        final float roundPx = (float) bm.getWidth() * 0.06f;
        roundDrawable.setCornerRadius(roundPx);
        return roundDrawable;
    }

    public void addListenerOnAddButton(Button add_button, int button_pressed) {
        add_button.setOnClickListener(new ButtonListener(button_pressed) {
            @Override
            public void onClick(View v) {
                button_clicked = this.button_pressed;
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GET_FROM_GALLERY);
            }
        });

    }

    public void addListenerOnDeleteButton(Button delete_button, int button_pressed){
        delete_button.setOnClickListener(new ButtonListener(button_pressed){
            @Override
            public void onClick(View v){
                int to_remove = button_pressed - 1;
                imported_images.remove(to_remove);
                imported_images.add(null);
                for(int i = to_remove; i < imported_images.size(); i++){

                        switch (i){
                            case 0:
                                setEditablePictureDrawableFromImportedImage(i, editable_picture_1);
                                break;
                            case 1:
                                setEditablePictureDrawableFromImportedImage(i, editable_picture_2);
                                break;
                            case 2:
                                setEditablePictureDrawableFromImportedImage(i, editable_picture_3);
                                break;
                            case 3:
                                setEditablePictureDrawableFromImportedImage(i, editable_picture_4);

                                break;
                        }


                }
            }
        });
    }

    public void setEditablePictureDrawableFromImportedImage(int position, EditablePicture editable_picture){
        if(imported_images.get(position) != null){
            editable_picture.getImageView().setImageDrawable(getDrawableFromEncodedString(imported_images.get(position)));
        }
        else {
            Bitmap bm = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
            bm.eraseColor(getResources().getColor(R.color.grey));
            RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), bm);
            final float roundPx = (float) bm.getWidth() * 0.15f;
            roundDrawable.setCornerRadius(roundPx);
            editable_picture.getImageView().setImageDrawable(roundDrawable);
        }
    }

    public void addListenerOnDoubleSeekBar(RangeSeekBar seekBar) {
        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Integer minValue, Integer maxValue) { // handle changed range values
                age_range_changed = true;
                age_min_tv.setText(String.valueOf(minValue));
                age_max_tv.setText(String.valueOf(maxValue));

            }

            @Override
            public void onTrackingTouch(RangeSeekBar bar, Integer minValue, Integer maxValue) {
                age_min_tv.setText(String.valueOf(minValue));
                age_max_tv.setText(String.valueOf(maxValue));
            }
        });
    }

    public void addListenerOnPrenom(EditText prenom_et) {
        prenom_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prenom_changed = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void addListenerOnDescription(EditText description_et) {
        description_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                description_changed = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void addListenerOnHommesCheckbox(CheckBox hommes_checkbox) {
        hommes_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    hommes_checked = true;
                } else {
                    hommes_checked = false;
                }
                preference_changed = true;
            }
        });
    }

    public void addListenerOnFemmesCheckbox(CheckBox femmes_checkbox) {
        femmes_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox) view).isChecked()) {
                    femmes_checked = true;
                } else {
                    femmes_checked = false;
                }
                preference_changed = true;
            }
        });
    }

    public void addListenerOnDistanceSeekbar(SeekBar distance_seekbar) {
        distance_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i == 0) {
                    i = 1;
                }
                String value = String.valueOf(i) + " km";
                distance_value.setText(value);
                distance_changed = true;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void addListenerOnSaveModificationsbutton(Button save_modifications) {
        save_modifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prenom_changed && prenom != prenom_et.getText().toString()) {
                    personne.setPrenom(prenom_et.getText().toString(), getActivity(), db);
                }
                if (distance_changed && distance_seekbar.getProgress() != personne.getDistance_max()) {
                    personne.setDistance_max(distance_seekbar.getProgress(), getActivity(), db);
                }
                if (preference_changed && checkIfPreferenceNeedsUpdating()) {
                    int preference_from_checkboxes = getPreferenceFromCheckboxes();
                    personne.setPreference(preference_from_checkboxes, getActivity(), db);
                }
                if (description_changed && description != description_et.getText().toString()) {
                    personne.setDescription(description_et.getText().toString(), getActivity(), db);
                }
                if (age_range_changed && (age_min != seekBar.getSelectedMinValue() || age_max != seekBar.getSelectedMaxValue())) {
                    personne.setAge_min(seekBar.getSelectedMinValue(), getActivity(), db);
                    personne.setAge_max(seekBar.getSelectedMaxValue(), getActivity(), db);
                }
                if (!pictures.equals(imported_images)) {
                    personne.setPhotos(pictures, imported_images, getActivity(), db);
                    pictures = imported_images;
                    //Checker s'il faut update et/ou add dans bd?

                    //si ya moins d'imported cest quya voulu en delete... on check s'il faut reset les premieres et delete le reste?

                }

                Toast toast = Toast.makeText(getActivity(), "Changements enregistrés ", Toast.LENGTH_LONG);
                toast.show();
            }
        });
    }

    public String encodeBitmapToBase64(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 50, baos);
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    public boolean checkIfPreferenceNeedsUpdating() {
        int initial_preference_from_db = personne.getPreference();
        int preference_from_checkboxes = getPreferenceFromCheckboxes();

        if (initial_preference_from_db == preference_from_checkboxes) {
            return false;
        } else {
            return true;
        }
    }

    public int getPreferenceFromCheckboxes() {
        if (hommes_checked && femmes_checked) {
            return 2;
        } else if (femmes_checked) {
            return 1;
        } else if (hommes_checked) { // Si les deux sont cochées ou aucun des deux n'est coché. Ou alors si rien n'est coché on met un msg d'erreur?
            return 0;
        } else {
            return 2;

        }
    }
}
