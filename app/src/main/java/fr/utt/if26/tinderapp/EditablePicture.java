package fr.utt.if26.tinderapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.graphics.ColorFilter;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.FileNotFoundException;
import java.io.IOException;

public class EditablePicture extends RelativeLayout {

    public ImageView iv;
    public Button add_button;
    public Button delete_button;
    public String encodedString;

    public Context c;


    public EditablePicture(Context context, AttributeSet attrs) {
        super(context, attrs);
        c = context;
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.editable_picture, this);

        iv = findViewById(R.id.image_view_1);
        add_button = findViewById(R.id.add_picture_button);
        delete_button = findViewById(R.id.delete_picture_button);

        add_button.setBackgroundResource(R.drawable.ic_plus);
        delete_button.setBackgroundResource(R.drawable.ic_delete);

        Bitmap bm = Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888);
        bm.eraseColor(getResources().getColor(R.color.grey));
        RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(getResources(), bm);
//roundDrawable.setCircular(true);
        final float roundPx = (float) bm.getWidth() * 0.15f;
        roundDrawable.setCornerRadius(roundPx);
        iv.setImageDrawable(roundDrawable);


        add_button = findViewById(R.id.add_picture_button);
        add_button.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View view) {

            }
        });

    }


    public String getEncodedString() {
        return encodedString;
    }

    public ImageView getImageView() {
        return iv;
    }

    public Button getAdd_button() {
        return add_button;
    }

    public Button getDelete_button() {
        return delete_button;
    }

}


