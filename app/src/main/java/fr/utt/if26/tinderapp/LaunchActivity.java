package fr.utt.if26.tinderapp;

import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.io.IOException;
import java.util.ArrayList;

public class LaunchActivity extends AppCompatActivity {

    DatabaseHelper mDBHelper;
    private SQLiteDatabase mDb;
    private ArrayList<Personne> personneArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        mDBHelper = new DatabaseHelper(this);
        personneArrayList = mDBHelper.getPersonnes();
        ArrayList<String> nomList = new ArrayList<String>();
        for (Personne personne : personneArrayList) {
            nomList.add(personne.getPrenom() + " " + personne.getNom());
        }

        Spinner spinner = (Spinner) findViewById(R.id.launch_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, nomList);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    public void confirmProfile(View view) {
        Spinner spinner = (Spinner) findViewById(R.id.launch_spinner);
        for (Personne personne : personneArrayList) {
            if ((personne.getPrenom() + " " + personne.getNom()).equals(spinner.getSelectedItem().toString())) {
                GlobalClass.setAuthenticated_user(personne);
            }
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
