package fr.utt.if26.tinderapp;

import android.app.Application;

public class GlobalClass extends Application {

    private static Personne authenticated_user;

    public static Personne getAuthenticated_user() {
        return authenticated_user;
    }

    public static void setAuthenticated_user(Personne authenticated_user) {
        GlobalClass.authenticated_user = authenticated_user;
    }

    // liste de tout le monde AUTRES que l'user?
    // liste des personnes qui likent l'user
}
