package fr.utt.if26.tinderapp;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TinderCardActivity extends AppCompatActivity {

    ViewPager viewPager;
    private Personne personne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tinder_card);
        Bundle b = getIntent().getExtras();
        boolean fromCards = false;
        int id = -1; // or other values
        if(b != null)
            id = b.getInt("id");
            fromCards = b.getBoolean("fromCards", false);
            DatabaseHelper db = new DatabaseHelper(getApplicationContext());
            personne = db.getPersonFromId(id);

        viewPager = (ViewPager) findViewById(R.id.viewPagerImages);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this, personne.getPhotos());

        viewPager.setAdapter(viewPagerAdapter);

        TextView prenom = (TextView) findViewById(R.id.prenom);
        String label = personne.getPrenom() + ", " + Utils.getAge(personne.getDate_naissance());
        prenom.setText(label);

        if (!fromCards) {
            findViewById(R.id.rejectBtn).setVisibility(View.GONE);
            findViewById(R.id.acceptBtn).setVisibility(View.GONE);
        }

        TextView description = (TextView) findViewById(R.id.description);
        description.setText(personne.getDescription());

        Personne profilActuel = GlobalClass.getAuthenticated_user();
        TextView distanceView = (TextView) findViewById(R.id.distance);
        int distance = Utils.distance(
                Double.parseDouble(personne.getLatitude()),
                Double.parseDouble(personne.getLongitude()),
                Double.parseDouble(profilActuel.getLatitude()),
                Double.parseDouble(profilActuel.getLongitude())
        );
        String distanceText = distance + " km";
        distanceView.setText(distanceText);

        findViewById(R.id.rejectBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("like", false);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        findViewById(R.id.acceptBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("like", true);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}
