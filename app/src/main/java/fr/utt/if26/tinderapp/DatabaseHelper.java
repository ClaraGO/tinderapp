package fr.utt.if26.tinderapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "tinderapp.db";
    //The Android's default system path of your application database.
    //public static final String DATABASE_FILEPATH = "/data/user/0/fr.utt.if26.tinderapp/databases/";


    private SQLiteDatabase currentDB = null;
    private boolean isCreating = false;


    private final Context myContext;


    private static final String TABLE_PERSONNES = "personnes";
    private static final String PERSONNE_ID = "personne_id";
    private static final String NOM = "nom";
    private static final String PRENOM = "prenom";
    private static final String DATE_NAISSANCE = "date_naissance";
    private static final String SEXE = "sexe";
    private static final String PSEUDO = "pseudo";
    private static final String DESCRIPTION = "description";
    private static final String LONGITUDE = "longitude";
    private static final String LATITUDE = "latitude";
    private static final String PREFERENCE = "preference";
    private static final String AGE_MIN = "age_min";
    private static final String AGE_MAX = "age_max";
    private static final String DISTANCE_MAX = "distance_max";

    private static final String TABLE_MATCHS = "matchs";
    private static final String LIKING_PERSON_ID = "liking_person_id";
    private static final String LIKED_PERSON_ID = "liked_person_id";
    private static final String TIMESTAMP = "timestamp";

    private static final String TABLE_PHOTOS = "photos";
    private static final String PHOTO_ID = "photo_id";
    private static final String FK_PERSONNE_ID = "personne_id";
    private static final String PHOTO_POSITION = "photo_position";
    private static final String PHOTO = "photo";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.myContext = context;
        // currentDB = get

        //
        //SQLiteDatabase db = getWritableDatabase();
        //onUpgrade(db,1,1);
        //onUpgrade(db, 1, 1);



    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        isCreating = true;
        currentDB = db;

        final String create_table_persons =
                "CREATE TABLE IF NOT EXISTS " + TABLE_PERSONNES + "("
                        + PERSONNE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + NOM + " text NOT NULL, "
                        + PRENOM + " text NOT NULL, "
                        + DATE_NAISSANCE + " text NOT NULL, "
                        + SEXE + " text NOT NULL, "
                        + PSEUDO + " text, "
                        + DESCRIPTION + " text, "
                        + LONGITUDE + " real, "
                        + LATITUDE + " real, "
                        + PREFERENCE + " integer NOT NULL DEFAULT 2, "
                        + AGE_MIN + " integer NOT NULL, "
                        + AGE_MAX + " integer NOT NULL, "
                        + DISTANCE_MAX + " integer NOT NULL)";
        currentDB.execSQL(create_table_persons);

        final String create_table_matchs =
                "CREATE TABLE IF NOT EXISTS " + TABLE_MATCHS + "("
                        + LIKING_PERSON_ID + " INTEGER, "
                        + LIKED_PERSON_ID + " INTEGER, "
                        + TIMESTAMP + " TEXT NOT NULL, "
                        + "PRIMARY KEY (" + LIKING_PERSON_ID + ", " + LIKED_PERSON_ID + "), "
                        + "FOREIGN KEY (" + LIKING_PERSON_ID + ") REFERENCES " + TABLE_PERSONNES + " (" + PERSONNE_ID + "), "
                        + "FOREIGN KEY (" + LIKED_PERSON_ID + ") REFERENCES " + TABLE_PERSONNES + " (" + PERSONNE_ID + "))";
        currentDB.execSQL(create_table_matchs);

        final String create_table_photos =
                "CREATE TABLE IF NOT EXISTS " + TABLE_PHOTOS + "("
                        + PHOTO_ID + " INTEGER PRIMARY KEY, "
                        + FK_PERSONNE_ID + " INTEGER, "
                        + PHOTO_POSITION + " INTEGER, "
                        + PHOTO + " text, "
                        + "FOREIGN KEY (" + FK_PERSONNE_ID + ") REFERENCES " + TABLE_PERSONNES + " (" + PERSONNE_ID + "))";

        currentDB.execSQL(create_table_photos);
        try {
            populateDatabase(currentDB);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        currentDB = sqLiteDatabase;
        currentDB.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONNES);
        currentDB.execSQL("DROP TABLE IF EXISTS " + TABLE_MATCHS);
        currentDB.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTOS);

        onCreate(currentDB);
    }


    public ArrayList<Personne> getPersonnes() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Personne> toReturn = new ArrayList<Personne>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PERSONNES, null);

        if (cursor.moveToFirst()) {
            do {
                ArrayList<Personne> likedPersonnes = getLikedPersonsFromPersonId(Integer.parseInt(cursor.getString(0)));
                ArrayList<String> photos = getPhotosFromPersonId(Integer.parseInt(cursor.getString(0)));
                Personne personne = new Personne(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                        cursor.getString(2), cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6), cursor.getString(7),
                        cursor.getString(8), Integer.parseInt(cursor.getString(9)), Integer.parseInt(cursor.getString(10)),
                        Integer.parseInt(cursor.getString(11)), Integer.parseInt(cursor.getString(12)), likedPersonnes, photos);
                toReturn.add(personne);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return toReturn;
    }

    public Personne getPersonFromId(int person_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Personne personne = new Personne();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_PERSONNES + " WHERE " + PERSONNE_ID + " = " + String.valueOf(person_id), null);
        if (cursor.moveToFirst()) {
            ArrayList<Personne> likedPersonnes = getLikedPersonsFromPersonId(Integer.parseInt(cursor.getString(0)));
            ArrayList<String> photos = getPhotosFromPersonId(Integer.parseInt(cursor.getString(0)));
            personne = new Personne(Integer.parseInt(cursor.getString(0)), cursor.getString(1),
                    cursor.getString(2), cursor.getString(3), cursor.getString(4),
                    cursor.getString(5), cursor.getString(6), cursor.getString(7),
                    cursor.getString(8), Integer.parseInt(cursor.getString(9)), Integer.parseInt(cursor.getString(10)),
                    Integer.parseInt(cursor.getString(11)), Integer.parseInt(cursor.getString(12)), likedPersonnes, photos);
        }
        cursor.close();
        db.close();
        return personne;
    }

    public ArrayList<Personne> getLikedPersonsFromPersonId(int liking_person_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Personne> likedPersons = new ArrayList<Personne>();
        //String query = "SELECT * FROM " + TABLE_MATCHS + " WHERE " + LIKING_PERSON_ID + "=?";
        String query = "SELECT * FROM " + TABLE_MATCHS + " LEFT JOIN " + TABLE_PERSONNES + " ON " +
                TABLE_MATCHS + "." + LIKED_PERSON_ID + " = " + TABLE_PERSONNES + "." + PERSONNE_ID +
                " WHERE " + TABLE_MATCHS + "." + LIKING_PERSON_ID + " =?";
        Cursor c = db.rawQuery(query, new String[]{String.valueOf(liking_person_id)});

        if (c.moveToFirst()) {
            do {
                ArrayList<String> photos = getPhotosFromPersonId(Integer.parseInt(c.getString(3)));
                //Log.d("MATCHHHH", c.getString(4) + " : "+c.getString(3));
                Personne personne = new Personne(Integer.parseInt(c.getString(3)), c.getString(4),
                        c.getString(5), c.getString(6), c.getString(7), c.getString(8), c.getString(9),
                        c.getString(10), c.getString(11), Integer.parseInt(c.getString(12)),
                        Integer.parseInt(c.getString(13)), Integer.parseInt(c.getString(14)),
                        Integer.parseInt(c.getString(15)), new ArrayList<Personne>(), photos);
                likedPersons.add(personne);
            } while (c.moveToNext());
        }
        return likedPersons;
    }

    public ArrayList<String> getPhotosFromPersonId(int person_id) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<String> toReturn = new ArrayList<String>();
        String query = "SELECT * FROM " + TABLE_PHOTOS + " WHERE " + FK_PERSONNE_ID + " = " + String.valueOf(person_id);
        Cursor cursor = db.rawQuery(query, null);

        if(cursor!=null && cursor.getCount() > 0){
            if (cursor.moveToFirst()) {
                do {
                    if(cursor.getString(3) != null){
                        toReturn.add(cursor.getString(cursor.getColumnIndex(PHOTO)));
                    }
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        //db.close();
        return toReturn;
    }

    @Override
    public synchronized void close() {
        if (currentDB != null)
            currentDB.close();
        super.close();
    }

    private void populateDatabase(SQLiteDatabase db) throws IOException {
        InputStream personsCsv = myContext.getAssets().open("persons_data.csv");
        CSVFile csvFileForPersons = new CSVFile(personsCsv);
        List resultPersons = csvFileForPersons.read();

        for (int i = 1; i < resultPersons.size(); i++) {
            String[] row = (String[]) resultPersons.get(i);
            ContentValues values = new ContentValues();
            values.put(NOM, row[0]);
            values.put(PRENOM, row[1]);
            values.put(DATE_NAISSANCE, row[2]);
            values.put(SEXE, row[3]);
            values.put(PSEUDO, row[4]);
            values.put(DESCRIPTION, row[5]);
            values.put(LONGITUDE, row[6]);
            values.put(LATITUDE, row[7]);
            values.put(PREFERENCE, row[8]);
            values.put(AGE_MIN, row[9]);
            values.put(AGE_MAX, row[10]);
            values.put(DISTANCE_MAX, row[11]);

            db.insert(TABLE_PERSONNES, null, values);
        }

        InputStream matchsCsv = myContext.getAssets().open("matchs_data.csv");
        CSVFile csvFileForMatchs = new CSVFile(matchsCsv);
        List resultMatchs = csvFileForMatchs.read();
        for (int j = 1; j < resultMatchs.size(); j++) {
            String[] row = (String[]) resultMatchs.get(j);
            ContentValues values = new ContentValues();
            values.put(LIKING_PERSON_ID, row[0]);
            values.put(LIKED_PERSON_ID, row[1]);
            Date now = new Date();
            values.put(TIMESTAMP, now.toString());

            db.insert(TABLE_MATCHS, null, values);
        }

        InputStream photosCsv = myContext.getAssets().open("photos_data.csv");
        CSVFile csvFileForPhotos = new CSVFile(photosCsv);
        List resultPhotos = csvFileForPhotos.read();
        for (int k = 1; k < resultPhotos.size(); k++) {
            String[] row = (String[]) resultPhotos.get(k);
            ContentValues values = new ContentValues();
            values.put(FK_PERSONNE_ID, row[0]);
            values.put(PHOTO_POSITION, row[1]);
            values.put(PHOTO, row[2]);

            db.insert(TABLE_PHOTOS, null, values);
        }
        //db.close();
    }

    public void updateSettingsInt(int personne_id, int value, String column_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "UPDATE " + TABLE_PERSONNES + " SET " + column_name + " = " + value
                + " WHERE " + PERSONNE_ID + " = " + personne_id;

        db.execSQL(query);
        db.close();

    }

    public void updateSettingsString(int personne_id, String value, String column_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "UPDATE " + TABLE_PERSONNES + " SET " + column_name + " = \"" + value
                + "\" WHERE " + PERSONNE_ID + " = " + personne_id;

        db.execSQL(query);
        db.close();

    }

    public void updatePictureAtPosition(int personne_id, int position, String encoded_image) {
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "UPDATE " + TABLE_PHOTOS + " SET " + PHOTO + " = \"" + String.valueOf(encoded_image)
                + "\" WHERE " + PERSONNE_ID + " = " + String.valueOf(personne_id) + " AND " + PHOTO_POSITION + " = "
                + position;
        db.execSQL(query);
        db.close();
    }

    public void insertPictureAtPosition(int personne_id, int position, String encoded_image) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FK_PERSONNE_ID, personne_id);
        values.put(PHOTO_POSITION, position);
        values.put(PHOTO, encoded_image);

        db.insert(TABLE_PHOTOS, null, values);
    }

    public void deletePictureAtPosition(int personne_id, int position) {
        SQLiteDatabase db = getWritableDatabase();

        db.delete(TABLE_PHOTOS,
                FK_PERSONNE_ID + " = ? AND " + PHOTO_POSITION + " = ?",
                new String[]{String.valueOf(personne_id), String.valueOf(position)});
    }

    public ArrayList<Personne> getPersonsWithinSettingsRange(int personne_id) {
        Personne person_searching = getPersonFromId(personne_id);
        ArrayList<Personne> allUsers = getPersonnes();
        ArrayList<Personne> personsWithinSettingsRange = new ArrayList<Personne>();
        for (Personne person : allUsers) {
            if (person.getId() != personne_id) {
                if (checkIfWithinSettings(person_searching, person) && checkIfWithinSettings(person, person_searching)) {
                    boolean alreadyLiked = false;
                    for(Personne personne : person_searching.getLiked_persons()){
                        if(personne.getId() == person.getId()){
                            alreadyLiked = true;
                        }
                    }
                    if (alreadyLiked) continue;
                    personsWithinSettingsRange.add(person);
                }
            }
        }
        return personsWithinSettingsRange;
    }

    private boolean checkIfWithinSettings(Personne person_searching, Personne person_toCheck) {
        boolean sexRange = false;
        boolean ageRange = false;
        //boolean distanceRange = false;
        switch (person_searching.getPreference()) {
            case 0:
                if (person_toCheck.getSexe().equals("m")) {
                    sexRange = true;
                }
                break;
            case 1:
                if (person_toCheck.getSexe().equals("f")) {
                    sexRange = true;
                }
                break;
            case 2:
                sexRange = true;
                break;
        }
        if (sexRange) {
            int personAge = Utils.getAge(person_toCheck.getDate_naissance());
            if (personAge <= person_searching.getAge_max() && personAge >= person_searching.getAge_min()) {
                ageRange = true;
            }
        }
        if (ageRange) {
            int distance = Utils.distance(Double.parseDouble(person_searching.getLatitude()),
                    Double.parseDouble(person_searching.getLongitude()),
                    Double.parseDouble(person_toCheck.getLatitude()),
                    Double.parseDouble(person_toCheck.getLongitude()));
            if (distance <= person_searching.getDistance_max()) {
                return true;
            }
        }
        return false;
    }

    public boolean likeWithPersonId(int personne_id, int liked_personne_id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(LIKING_PERSON_ID, personne_id);
        values.put(LIKED_PERSON_ID, liked_personne_id);
        Date now = new Date();
        values.put(TIMESTAMP, now.toString());

        db.insert(TABLE_MATCHS, null, values);

        db.close();
        return checkMatch(personne_id, liked_personne_id);
    }

    public boolean checkMatch(int personne_id, int liked_personne_id) {
        ArrayList<Personne> likedPersonnes = getLikedPersonsFromPersonId(liked_personne_id);
        boolean match = false;
        for (Personne personne : likedPersonnes) {
            if (personne.getId() == personne_id) {
                match = true;
            }
        }

        return match;
    }

    public ArrayList<Personne> getMatchFromPersonneId(int personne_id) {
        ArrayList<Personne> likedPersons = getLikedPersonsFromPersonId(personne_id);
        ArrayList<Personne> matchs = new ArrayList<Personne>();
        for(Personne personne : likedPersons) {
            if (checkMatch(personne_id, personne.getId())) {
                matchs.add(personne);
            }
        }
        return matchs;
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        // TODO Auto-generated method stub
        if (isCreating && currentDB != null) {
            return currentDB;
        }
        return super.getWritableDatabase();
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        // TODO Auto-generated method stub
        if (isCreating && currentDB != null) {
            return currentDB;
        }
        return super.getReadableDatabase();
    }
}