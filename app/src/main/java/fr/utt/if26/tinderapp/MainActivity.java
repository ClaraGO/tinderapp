package fr.utt.if26.tinderapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class MainActivity extends AppCompatActivity {
    FragmentPagerAdapter adapterViewPager;
    private ProfileFragment m1stFragment;
    private CardsFragment m2ndFragment;
    private MatchsFragment m3rdFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CustomViewPager vpPager = (CustomViewPager) findViewById(R.id.vpPager);

        adapterViewPager = new SampleAdapter(getSupportFragmentManager());
        vpPager.setPagingEnabled(false);
        vpPager.setAdapter(adapterViewPager);
        vpPager.setCurrentItem(1);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.view_pager_tab);
        tabLayout.setupWithViewPager(vpPager);
    }

    public class SampleAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 3;

        private SampleAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Here we can finally safely save a reference to the created
        // Fragment, no matter where it came from (either getItem() or
        // FragmentManger). Simply save the returned Fragment from
        // super.instantiateItem() into an appropriate reference depending
        // on the ViewPager position.
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
            // save the appropriate reference depending on position
            switch (position) {
                case 0:
                    m1stFragment = (ProfileFragment) createdFragment;
                    break;
                case 1:
                    m2ndFragment = (CardsFragment) createdFragment;
                    break;
                case 2:
                    m3rdFragment = (MatchsFragment) createdFragment;
            }
            return createdFragment;
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return ProfileFragment.newInstance(0, "Profile");
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return CardsFragment.newInstance(1, "Cartes");
                case 2: // Fragment # 1 - This will show SecondFragment
                    return MatchsFragment.newInstance(2, "Matchs");
                default:
                    return null;
            }
        }

        // Generate title based on item position
        @Override
        public CharSequence getPageTitle(int position) {
            Drawable drawable = getResources().getDrawable(R.drawable.ic_burn);
            ImageSpan span;
            SpannableStringBuilder sb = new SpannableStringBuilder(" ");
            switch (position) {
                case 0:
                    drawable = getResources().getDrawable(R.drawable.ic_user);
                    sb = new SpannableStringBuilder(" ");
                    break;
                case 1:
                    drawable = getResources().getDrawable(R.drawable.ic_burn);
                    sb = new SpannableStringBuilder(" ");
                    break;
                case 2:
                    drawable = getResources().getDrawable(R.drawable.ic_speech_bubble);
                    sb = new SpannableStringBuilder(" ");
                    break;
            }
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            span = new ImageSpan(drawable);
            sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            return sb;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0 && resultCode == Activity.RESULT_OK) {
            boolean result = data.getBooleanExtra("like", false);
            if (result) {
                m2ndFragment.swipeRight();
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.detach(m3rdFragment);
                ft.attach(m3rdFragment);
                ft.commit();
            } else {
                m2ndFragment.swipeLeft();
            }
        }
    }
}

