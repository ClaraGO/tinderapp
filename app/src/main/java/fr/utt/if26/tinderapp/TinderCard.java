package fr.utt.if26.tinderapp;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;

@Layout(R.layout.tinder_card_view)
public class TinderCard {

    @View(R.id.profileImageView)
    private ImageView profileImageView;

    @View(R.id.nameAgeTxt)
    private TextView nameAgeTxt;

    @View(R.id.locationNameTxt)
    private TextView locationNameTxt;

    private Personne mPersonne;
    private Personne mProfilActuel;
    private Activity mActivity;
    private SwipePlaceHolderView mSwipeView;

    public TinderCard(Activity activity, Personne personne, Personne profilActuel, SwipePlaceHolderView swipeView) {
        mActivity = activity;
        mPersonne = personne;
        mSwipeView = swipeView;
        mProfilActuel = profilActuel;
    }

    @Resolve
    private void onResolved(){
        byte[] decodedString = Base64.decode(mPersonne.getPhotos().get(0), Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        profileImageView.setImageBitmap(decodedByte);

        int age = mPersonne.getAge() != 0 ? mPersonne.getAge() : Utils.getAge(mPersonne.getDate_naissance());
        String text = mPersonne.getPrenom() + ", " + age;
        nameAgeTxt.setText(text);
        String distanceText;
        if (mPersonne.getLocation() != null && !mPersonne.getLocation().isEmpty()) {
            distanceText = mPersonne.getLocation();
        } else {
            int distance = Utils.distance(
                    Double.parseDouble(mPersonne.getLatitude()),
                    Double.parseDouble(mPersonne.getLongitude()),
                    Double.parseDouble(mProfilActuel.getLatitude()),
                    Double.parseDouble(mProfilActuel.getLongitude())
            );
            distanceText = distance + " km";
        }

        locationNameTxt.setText(distanceText);
    }

    @Click(R.id.profileImageView)
    public void onProfileImageViewClick() {

        ActivityOptions options = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Bundle bundle = ActivityOptions.makeSceneTransitionAnimation((Activity) mActivity, (android.view.View) profileImageView, "profile").toBundle();
            //Intent intent = TinderCardActivity.makeIntent(mContext, album);
            Intent intent = new Intent(mActivity.getApplicationContext(), TinderCardActivity.class);
            bundle.putInt("id", mPersonne.getId()); //Your id
            bundle.putBoolean("fromCards", true);
            intent.putExtras(bundle); //Put your id to your next Intent
            mActivity.startActivityForResult(intent, 0, bundle);
        } else {
            Intent intent = new Intent(mActivity.getApplicationContext(), TinderCardActivity.class);
            Bundle b = new Bundle();
            b.putInt("id", mPersonne.getId()); //Your id
            b.putBoolean("fromCards", true);
            intent.putExtras(b); //Put your id to your next Intent
            mActivity.startActivityForResult(intent, 0);
        }

        Log.d("DEBUG", "onProfileImageViewClick");
    }

    @SwipeOut
    private void onSwipedOut(){
        Log.d("EVENT", "onSwipedOut");
        mSwipeView.addView(this);
    }

    @SwipeCancelState
    private void onSwipeCancelState(){
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn(){
        Log.d("EVENT", "onSwipedIn");
        DatabaseHelper db = new DatabaseHelper(mActivity.getApplicationContext());

        if (db.likeWithPersonId(mProfilActuel.getId(), mPersonne.getId())) {
            new AlertDialog.Builder(new ContextThemeWrapper(mActivity, R.style.AppTheme_Main))
                    .setTitle("C'est un match !!!")
                    .setMessage("Voulez-vous voir son profil ?")
                    .setPositiveButton("Allons-y Alonzo", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(mActivity.getApplicationContext(), TinderCardActivity.class);
                            Bundle b = new Bundle();
                            b.putInt("id", mPersonne.getId()); //Your id
                            intent.putExtras(b); //Put your id to your next Intent
                            mActivity.getApplicationContext().startActivity(intent);
                        }
                    })
                    .setNegativeButton("Abort", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.d("MainActivity", "Aborting mission...");
                        }
                    })
                    .show();
            // find your fragment
            FragmentActivity f = (FragmentActivity) mActivity;
            MatchsFragment matchsFragment = (MatchsFragment) f.getSupportFragmentManager().findFragmentByTag("android:switcher:"+R.id.vpPager+":2");
            // update the list view
            matchsFragment.updateAdapter();
        }
    }

    @SwipeInState
    private void onSwipeInState(){
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState(){
        Log.d("EVENT", "onSwipeOutState");
    }
}
