package fr.utt.if26.tinderapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CardsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CardsFragment extends Fragment {
    // Store instance variables
    private String title;
    private int page;
    private SwipePlaceHolderView mSwipeView;
    private ArrayList<Personne> personneArrayList;
    private Personne profilActuel;
    private View view;

    // newInstance constructor for creating fragment with arguments
    public static CardsFragment newInstance(int page, String title) {
        CardsFragment cardsFragment = new CardsFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", page);
        args.putString("someTitle", title);
        cardsFragment.setArguments(args);
        return cardsFragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());
        this.profilActuel = GlobalClass.getAuthenticated_user();
        this.personneArrayList = db.getPersonsWithinSettingsRange(this.profilActuel.getId());
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        view = inflater.inflate(R.layout.fragment_cards, container, false);

        mSwipeView = (SwipePlaceHolderView) view.findViewById(R.id.swipeView);

        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.tinder_swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.tinder_swipe_out_msg_view));

        for (Personne personne : personneArrayList) {
            // On ajoute une tinderCard ici avec un profile
            mSwipeView.addView(new TinderCard(getActivity(), personne, profilActuel, mSwipeView));
        }

        view.findViewById(R.id.rejectBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeView.doSwipe(false);
            }
        });

        view.findViewById(R.id.acceptBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSwipeView.doSwipe(true);
            }
        });

        //TextView cardsLabel = (TextView) view.findViewById(R.id.cards_label);
        //cardsLabel.setText(page + " -- " + title);
        return view;
    }

    public void swipeLeft() {
        mSwipeView.doSwipe(false);
    }

    public void swipeRight() {
        mSwipeView.doSwipe(true);
    }
}
